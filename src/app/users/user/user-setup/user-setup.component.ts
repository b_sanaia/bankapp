import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {UserService} from '../../services/user.service';
import {DestroyService} from '../../../shared/services/destroy.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/reducers';
import {UserActions} from '../../store/action-types';
import {showSuccessModal} from '../../store/user.selectors';
import {SuccessDialogComponent} from '../../../shared/components/success-dialog/success-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-user-setup',
  templateUrl: './user-setup.component.html',
  styleUrls: ['./user-setup.component.scss'],
})
export class UserSetupComponent extends DestroyService implements OnInit {
  userData: any;

  constructor(private router: ActivatedRoute,
              private userService: UserService,
              private store: Store<AppState>,
              private dialog: MatDialog,
  ) { super(); }

  ngOnInit(): void {
    this.router.data.pipe(takeUntil(this.destroy$)).subscribe((data: Data) => {
      this.userData = data.user;
    });

    this.store.select(showSuccessModal).pipe(takeUntil(this.destroy$))
      .subscribe((show) => {
        if (show) {
          this.openSuccessModal();
        }
      });

    this.dialog.afterAllClosed.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(UserActions.showSuccessModal({show: false}));
    });
  }

  private openSuccessModal(): void {
    this.dialog.open(SuccessDialogComponent, {data: 'Save Changes Success'});
  }


  toggleUserStatus(ev) {
    this.userData.active = ev.checked;
    this.userService.toggleUserStatus({id: this.userData.id, checked: ev.checked}).pipe(takeUntil(this.destroy$)).subscribe();
  }

  updateUser(value) {
    this.userService.updateUser(value, this.userData.id).pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.store.dispatch(UserActions.showSuccessModal({show: true}));
      });
  }
}
