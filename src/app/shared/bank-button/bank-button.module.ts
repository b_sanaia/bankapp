import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankButtonComponent } from './bank-button.component';



@NgModule({
  declarations: [
    BankButtonComponent
  ],
  exports: [
    BankButtonComponent
  ],
  imports: [
    CommonModule
  ]
})
export class BankButtonModule { }
