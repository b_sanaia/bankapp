import {AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { UserService} from '../../services/user.service';
import { MatDialog} from '@angular/material/dialog';
import { InviteNewUserComponent} from '../invite-new-user/invite-new-user.component';
import { InviteNewUserDialogService} from '../../services/invite-new-user-dialog.service';
import { DeleteUserComponent} from '../delete-user/delete-user.component';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {DestroyService} from '../../../shared/services/destroy.service';
import {takeUntil} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/reducers';
import { showSuccessModal} from '../../store/user.selectors';
import {SuccessDialogComponent} from '../../../shared/components/success-dialog/success-dialog.component';
import {UserActions} from '../../store/action-types';
import {MatPaginator} from '@angular/material/paginator';


@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent extends DestroyService implements OnInit {
  displayedColumns: string[] = ['name', 'weight', 'symbol', 'actions'];
  dataSource: any;
  dataSourceCopy: any;
  deleteDialogRef: any;

  pageIndex = 0;
  pageSize = 5;
  lowValue = 0;
  highValue = 5;

  @ViewChild(MatPaginator) paginator: MatPaginator;


  getPaginatorData(event) {
    if (event.pageIndex === this.pageIndex + 1) {
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue = this.highValue + this.pageSize;
    } else if (event.pageIndex === this.pageIndex - 1) {
      this.lowValue = this.lowValue - this.pageSize;
      this.highValue = this.highValue - this.pageSize;
    }
    this.pageIndex = event.pageIndex;
    this.navigate(this.pageIndex);
  }

  private navigate(params): void {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {page: params},
        queryParamsHandling: 'merge',
      });
  }

  constructor(private userService: UserService,
              private dialog: MatDialog,
              private dialogService: InviteNewUserDialogService,
              private router: Router,
              private store: Store<AppState>,
              private activatedRoute: ActivatedRoute
  ) {
    super();
  }

  ngOnInit(): void {
    this.getUsers();
    this.userService.reload.subscribe(() => {
      this.deleteDialogRef?.close();
      this.getUsers();
    });
    this.dialogService.sendInvite.subscribe(data => {
      this.userService.senUserInvitation(data).subscribe(res => {
        this.getUsers();
        this.store.dispatch(UserActions.showSuccessModal({show: true}));
      });
    });

    this.store.select(showSuccessModal).pipe(takeUntil(this.destroy$))
      .subscribe((show) => {
        if (show) {
          this.openSuccessModal();
        }
      });

    this.dialog.afterAllClosed.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.store.dispatch(UserActions.showSuccessModal({show: false}));
    });

    setTimeout(() => {
      this.activatedRoute.queryParams.pipe(takeUntil(this.destroy$))
        .subscribe((params: Params) => {
          this.paginator.pageIndex = +params.page;
          this.paginator.page.next({
            pageIndex: +params.page,
            pageSize: this.paginator.pageSize,
            length: this.paginator.length
          });
        });
    });

  }

  public openSuccessModal(): void {
    this.dialog.open(SuccessDialogComponent, {data: 'User Add Success'});
  }

  filterTable(val) {
    this.dataSourceCopy = this.dataSource.filter(x => x.firstName.toLowerCase().includes(val) ||
      x.lastName.toLowerCase().includes(val) || x.email.toLowerCase().includes(val) ||
      x.role.toLowerCase().includes(val)
    );
  }

  public getUsers(): void {
    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => {
        this.dataSource = users;
        this.activatedRoute.queryParams.pipe(takeUntil(this.destroy$))
          .subscribe((params) => {
            if (this.dataSource) {
              this.filterTable(params.query);
            }
          });
      });
  }

  public toggleUserStatus(checked, id): void {
    this.dataSource[this.dataSource.findIndex(user => user.id === id)].active = checked.checked;
    this.userService.toggleUserStatus({id, checked: checked.checked}).subscribe();
  }

  public onRemoveUserClick(user): void {
    this.deleteDialogRef = this.dialog.open(DeleteUserComponent,
      {data: {user}});
  }

  public inviteNewUser(): void {
    const dialogRef = this.dialog.open(InviteNewUserComponent);
    this.dialogService.close.pipe(takeUntil(this.destroy$)).subscribe(() => {
      dialogRef.close();
    });
  }

  public openProfile(element): void {
    this.router.navigate(['/user-profile', element.id],
    );
  }
}

