import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {AppConfigurationService} from '../../shared/services/app-configuratin.service';

@Injectable()
export class UserService {

  reload: Subject<void> = new Subject<void>();

  baseUrl = this.appConfig.baseUrl;

  constructor(private http: HttpClient, private appConfig: AppConfigurationService) { }

  getUsers() {
    return this.http.get(`${this.baseUrl}/users`);
  }

  getUser(id) {
    return this.http.get(`${this.baseUrl}/users/${id}`);
  }

  updateUser(data, id) {
    return this.http.patch(`${this.baseUrl}/users/${id}`, data);
  }


  toggleUserStatus(data) {
    return this.http.patch(`${this.baseUrl}/users/${data.id}`, {active: data.checked});
  }

  senUserInvitation(data) {
    return this.http.post(`${this.baseUrl}/users`, data);
  }

  removeUser(id) {
    return this.http.delete(`${this.baseUrl}/users/${id}`);
  }

}
