import {createReducer, on} from '@ngrx/store';
import {UserActions} from '../action-types';

export const userStateFeatureKey = 'userState';

export interface UserState {
  user: number;
  showSuccessModal: boolean;
}

export const initialUserState: UserState = {
  user: 2,
  showSuccessModal: false,
};

export const userReducer = createReducer(
  initialUserState,
  on(UserActions.showSuccessModal, (state, action) => {
    return {
      ...state,
      showSuccessModal: action.show
    };
  }),
);
