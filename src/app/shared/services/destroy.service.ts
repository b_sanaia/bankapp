import {Injectable, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class DestroyService implements OnDestroy {
  public destroy$: Subject<void> = new Subject<void>();
  constructor() {}
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
