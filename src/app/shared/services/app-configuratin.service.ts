import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class AppConfigurationService {

  private appConfig: any;

  constructor(private http: HttpClient) {
  }

  public loadAppConfig(): Promise<any> {
    return this.http.get('../../assets/config/app-config.json')
      .toPromise()
      .then(data => {
        this.appConfig = data;
      });
  }

  get baseUrl(): any {
    if (!this.appConfig) {
      throw Error('Config file not loaded!');
    }
    return this.appConfig.baseUrl;
  }
}
