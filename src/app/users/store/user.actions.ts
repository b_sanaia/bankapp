import {createAction, props} from '@ngrx/store';

export const showSuccessModal = createAction(
  '[User] Show Success Modal',
  props<{show: boolean}>()
);
