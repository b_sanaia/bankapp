import {createFeatureSelector, createSelector} from '@ngrx/store';
import {UserState, userStateFeatureKey} from './reducers';

export const selectUserState = createFeatureSelector<UserState>(userStateFeatureKey);


export const showSuccessModal = createSelector(
  selectUserState,
  state => state.showSuccessModal
);
