import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import {UserTableComponent} from './user/user-table/user-table.component';
import {UserSetupComponent} from './user/user-setup/user-setup.component';
import {UserSetupResolveService} from './services/user-setup.resolve.service';

const routes: Routes = [
  { path: '', component: UserTableComponent },
  { path: 'user-profile/:id', component: UserSetupComponent, resolve: {user: UserSetupResolveService} },
];

@NgModule({
  declarations: [ ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
})
export class UserRoutingModule { }
