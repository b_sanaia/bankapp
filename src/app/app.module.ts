import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import {AppConfigurationService} from './shared/services/app-configuratin.service';
import {StoreModule} from '@ngrx/store';
import {reducers, metaReducers} from './store/reducers';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

const routes: Routes = [
    {path: '', loadChildren: () => import('./users/users.module').then(m => m.UsersModule)},
  ];

@NgModule({
  declarations: [ AppComponent ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'}),
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigurationService],
      useFactory: (appConfigService: AppConfigurationService) => {
        return () => {
          return appConfigService.loadAppConfig();
        };
      }
    }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
