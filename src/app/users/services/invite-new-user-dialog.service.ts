import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable()
export class InviteNewUserDialogService {
  close: Subject<void> = new Subject<void>();
  sendInvite: Subject<void> = new Subject<void>();
}
