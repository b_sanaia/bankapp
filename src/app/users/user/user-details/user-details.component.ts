import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {InviteNewUserDialogService} from '../../services/invite-new-user-dialog.service';

@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() btnSize: 'small' | 'large' = 'small';
  @Input() btnText: string;
  @Input() formValue: any;
  @Output() btnClicked: EventEmitter<any> = new EventEmitter<any>();

  addUserForm: FormGroup;
  public get accounts() {
    return this.addUserForm.get('accounts') as FormArray;
  }

  public static createAccount(): FormGroup {
    return new FormGroup({
      accountNumber: new FormControl('', [Validators.required]),
      currency: new FormControl('', [Validators.required])
    });
  }

  public addAccount(): void {
    this.accounts.push(UserDetailsComponent.createAccount());
  }

  public removeAccount(idx): void {
    this.accounts.removeAt(idx);
  }

  public addUser(): void {
    if (this.addUserForm.invalid) {
      return;
    }
    this.dialogService.sendInvite.next(this.addUserForm.value);
    this.addUserForm.reset();
    this.dialogService.close.next();
  }

  public onBtnClick(): void {
    if (this.addUserForm.invalid) {
      return;
    }
    this.btnClicked.emit(this.addUserForm.value);
  }

  constructor(private dialogService: InviteNewUserDialogService
  ) {
    this.addUserForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9!#$%&\'*+\\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$')
      ]),
      firstName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      phone: new FormControl('', [Validators.required]),
      personalId: new FormControl('', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]),
      gender: new FormControl('', [Validators.required]),
      address: new FormGroup({
        country: new FormControl(''),
        city: new FormControl(''),
        street: new FormControl(''),
        zipCode: new FormControl(''),
      }),
      accounts: new FormArray([UserDetailsComponent.createAccount()]),
      role: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void { }

  ngOnDestroy() {
    this.btnClicked.complete();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.formValue?.currentValue) {
      this.addUserForm.patchValue(changes?.formValue?.currentValue);
    }
  }

}
