import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {DestroyService} from '../../services/destroy.service';
import {takeUntil} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/reducers';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends DestroyService implements OnInit {
  @Input() title: string;
  @Input() showSearch: boolean;
  filterTable = new FormControl();

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit(): void {
    const query = localStorage.getItem('query');
    if (query) {
      this.filterTable.setValue(query);
      this.navigate(query);
    }

    this.filterTable.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(val => {
        localStorage.setItem('query', val);
        this.navigate(val);
      });
  }

  private navigate(params): void {
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {query: params},
        queryParamsHandling: 'merge',
      });
  }

}
