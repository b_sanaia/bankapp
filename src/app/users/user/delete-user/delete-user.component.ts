import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {takeUntil} from 'rxjs/operators';
import {UserService} from '../../services/user.service';
import {DestroyService} from '../../../shared/services/destroy.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent extends DestroyService implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService
  ) { super(); }

  ngOnInit(): void { }

  removeUser() {
    this.userService.removeUser(this.data.user.id).pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.userService.reload.next();
    });
  }
}
