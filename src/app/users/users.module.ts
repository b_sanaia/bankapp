import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {BankButtonModule} from '../shared/bank-button/bank-button.module';
import {InviteNewUserDialogService} from './services/invite-new-user-dialog.service';
import {UserService} from './services/user.service';
import {UserRoutingModule} from './user-routing.module';
import {UserSetupResolveService} from './services/user-setup.resolve.service';
import {DeleteUserComponent} from './user/delete-user/delete-user.component';
import {InviteNewUserComponent} from './user/invite-new-user/invite-new-user.component';
import {UserSetupComponent} from './user/user-setup/user-setup.component';
import {UserTableComponent} from './user/user-table/user-table.component';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import {StoreModule} from '@ngrx/store';
import * as fromUserState from './store/reducers';
import {userReducer} from './store/reducers';
import {MatPaginatorModule} from "@angular/material/paginator";


@NgModule({
  declarations: [
    DeleteUserComponent,
    InviteNewUserComponent,
    UserSetupComponent,
    UserTableComponent,
    UserDetailsComponent,
  ],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,
    BankButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatTableModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    UserRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    StoreModule.forFeature(fromUserState.userStateFeatureKey, userReducer),
    MatPaginatorModule
  ],
  providers: [
    InviteNewUserDialogService,
    UserService,
    UserSetupResolveService,
  ]
})
export class UsersModule { }
