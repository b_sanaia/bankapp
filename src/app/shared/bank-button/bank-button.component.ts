import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'bank-button',
  templateUrl: './bank-button.component.html',
  styleUrls: ['./bank-button.component.scss']
})
export class BankButtonComponent implements OnInit {

  @Input() type: 'primary' | 'error' = 'primary';
  @Input() size: 'small' | 'large' = 'large';

  constructor() { }

  ngOnInit(): void { }

}
