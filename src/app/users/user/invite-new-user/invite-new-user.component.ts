import { Component, OnInit } from '@angular/core';
import {InviteNewUserDialogService} from '../../services/invite-new-user-dialog.service';

@Component({
  selector: 'app-invite-new-user',
  templateUrl: './invite-new-user.component.html',
  styleUrls: ['./invite-new-user.component.scss']
})
export class InviteNewUserComponent implements OnInit {

  public addUser(value): void {
    this.dialogService.sendInvite.next(value);
    this.dialogService.close.next();
  }

  constructor(private dialogService: InviteNewUserDialogService) { }

  ngOnInit(): void { }
}
